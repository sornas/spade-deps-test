.PHONY: all build clean setup update

all: build

setup:
	(cd swim && \
		cargo build \
	)

build: setup
	swim/target/debug/swim build
clean: setup
	swim/target/debug/swim clean
	rm -rf swim.log
update: setup
	swim/target/debug/swim update
